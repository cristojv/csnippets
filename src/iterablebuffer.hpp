/**
 * \file IterableBuffer.hpp
 * \version 1.1.1
 * \brief Generic Iterable buffer implementation for embedded targets
 * \author Cristo Manuel Jurado Verdu
 * \company IDeTIC
 * 
 * Iterable buffer works in this way:
 * There is 2 index pointers: insertIndex and readIndex.
 * ->insertIndex: Points to the next available position in buffer for insert elements
 *                Is there is not more room for the element then it returns false.
 * ->readIndex: Points to the next readable element. This buffer allows to read one by
 *              one the elements, saving the current state. If it's necesesary to
 *              re-read the elements it's neccesary to use the function resetRead.
*/

#ifndef ITERABLE_BUFFER_H //Header guards
#define ITERABLE_BUFFER_H
#include <stdint.h> //includes in alphabetical order
                    //bundled libraries includes first
                    //library includes after

namespace CSnippets { //Namespace declaration

template<typename type, uint8_t bufferSize>

class IterableBuffer {
  
  public:
    
    /*!
     * \brief Intentionally empty constructor - nothing to allocate
     * \warning If class is instantiated on stack, heap or inside noinit section
     * then the buffer has to be explicitly cleared before use
     */
   IterableBuffer(){
      _readIndex = 0;
      _insertIndex = 0;
      _count = 0;
   };

    /*!
     * \brief Intentionally emtpy destructor - nothing has to be released
     */
    ~IterableBuffer(){
    };

    bool isFull(){
      return (_count >= bufferSize);
    }

    bool isEmpty(){
      return (_readIndex==_insertIndex && _count == 0);
    }

    bool hasBeenRead(){
      return (_readIndex==_insertIndex && _count !=0);
    }
    
    int insert(type data){
      if(!isFull()){
        _iterableBuffer[_insertIndex] = data;
        _insertIndex++;
        _count++;
        return true;
      }
      return false;
    }

    int readNext(type *dataPointer){
      if(isEmpty()){
        return false;
      }else if(hasBeenRead()){
        return false;
      }else{
        *dataPointer = _iterableBuffer[_readIndex];
        _readIndex++;
        return true;
      }
    }

    void resetRead(){
      _readIndex = 0;
    }

    void reset(){
      _readIndex = 0;
      _insertIndex = 0;
      _count = 0;
    }
    
  private:
    type volatile _insertIndex;
    type volatile _readIndex;
    type _iterableBuffer[bufferSize];
    uint8_t _count;
};

}

#endif